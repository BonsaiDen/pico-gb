#ifndef _MSC_H_
#define _MSC_H_

struct File {
    char name[9];
    char ext[4];
    uint32_t size;
    uint8_t *data;
};

extern void msc_init(const char *volume_label, const char *readme_text, const uint32_t max_upload_size_kb);
extern void msc_task(void);
extern void msc_transfer_start_callback(void);
extern void msc_transfer_complete_callback(struct File file);
extern void msc_ejected_callback(void);

#endif


#ifndef _CART_LIB_H_
#define _CART_LIB_H_

// External -------------------------------------------------------------------
// ----------------------------------------------------------------------------
#include "pico/stdlib.h"


// Global Data ----------------------------------------------------------------
// ----------------------------------------------------------------------------
extern uint8_t* ROM_DATA;
extern uint8_t* RAM_DATA;


// Global Interface -----------------------------------------------------------
// ----------------------------------------------------------------------------
extern void cart_init(void);
extern void cart_reset(void);
extern void cart_init_core(void);
extern bool cart_is_running(void);
extern void cart_led_write(const uint8_t m);

#endif


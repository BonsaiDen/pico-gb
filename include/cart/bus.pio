; Constants
; ---------
.define PUBLIC A15_PIN          15
.define PUBLIC WR_PIN           24
.define PUBLIC PHI_PIN          25
.define PUBLIC RESET_PIN        26
.define PUBLIC CS_PIN           28


; External Bus Timing Diagramm (referenced below)
;
; Credit to gekkio and aappleby
;
; https://github.com/Gekkio/gb-ctr
; https://github.com/aappleby/MetroBoy
;
;
; # High Level Explanation
;
; 1a) Bus program waits for PHI=HIGH, sets PINDIRS=IN, clears IRQ 1 & 2,
;     sets and waits for IRQ 0 to be cleared
;
; 1b) Bus program checks RD and delays for 16 cycles to ensure address is valid
;
; 1c) If RD=LOW, bus rogram reads the entire bus,
;     sending it to the C program, waiting for data to be returned,
;     then puts the data onto the bus
;
; 1d) Bus program waits for PHI=LOW
;
; 1e) Gameboy fetches value from data bus
;
; 1f) If RD=HIGH, bus program waits for WR=LOW, reads the entire bus,
;     sending to the the C program
;
; 2a) Rom access program sets and waits for IRQ 1 to be cleared
; 2b) Rom access program waits for A15=LOW then clears IRQ 0 then loops
;
; 3a) Rom access program sets and waits for IRQ 1 to be cleared
; 3b) Rom access program checks if A14=LOW and if so, clears IRQ 0 then loops
;
; CPU Cycle   ┆1111222233334444┆    (e.g. ld a,b would take 4 cycles, each cycle takes 2 phases)
;             ┆                ┆    (each cycle is 500ns long on GBC)
;             ┆                ┆
; CPU Phase   ┆AABBCCDDEEFFGGHH┆    (phase changes on each edge)
;             ┆                ┆    (each phase is 250ns long on GBC)
;             ┆                ┆
;             ┆┌─┐ ┌─┐ ┌─┐ ┌─┐ ┌╶╶╶ (4.19 mhz / 8.38 mhz)
;       CLK ╴╴╴┘ └─┘ └─┘ └─┘ └─┘    ( 238 ns  /  119 ns )
;             ┆                ┆
;             ┆1a   1b         ┆
;             ┆     1c         ┆
;             ┆         1d     ┆
;             ┆┌───────┐       ┌╶╶╶ (1.0475 mhz / 2.095 mhz)
;       PHI ╴╴╴┘       └───────┘    (   954 ns  /   477 ns )
;             ┆ 1000ns  1000ns ┆    (   381 cyc /   190 cyc)
;             ┆                ┆
; <ADDRESS> ╴╴╴──██████████████╶╶╶╶
;             ┆  1750ns        ┆
;             ┆            1e  ┆
;    <DATA> ╴╴╴────████████────╶╶╶╶ (READ)
;             ┆    1000ns      ┆
;        __ ╴┐┆                ┌╶╶╶
;        RD  └─────────────────┘
;             ┆                ┆
;             ┆         1f     ┆
;        __ ╴╴╴────────┐     ┌─╶╶╶
;        WR   ┆        └─────┘ ┆
;             ┆         750ns  ┆
;    <DATA> ╴╴╴────────████████╶╶╶  (WRITE)
;             ┆        1000ns  ┆
;             ┆2a              ┆
;             ┆┌───┐           ┌╶╶╶
;       A15 ╴╴╴┘   └───────────┘
;             ┆ 500ns          ┆
;             ┆                ┆
;             ┆┌───┐2b         ┌╶╶╶
;       A14 ╴╴╴┘   └───────────┘
;             ┆ 500ns          ┆
;             ┆                ┆
;             ┆3a              ┆
;        __   ┆┌───┐3b         ┌╶╶╶
;        CS ╴╴╴┘   └───────────┘
;             ┆ 500ns          ┆
.program bus
bus_start:
    ; Wait for reset to be released
    wait 0 gpio RESET_PIN
.wrap_target
bus_wait:
    ; wait for start of clock cycle
    wait 1 gpio PHI_PIN

    ; Change IO dir of ALL pins to INPUT
    mov osr, null
    out pindirs, 32

    ; Clear IRQs, waking of the access programs
    irq clear 1
    irq clear 2

    ; Set IRQ and wait for access programs to clear it
    irq wait 0

    ; If RD is HIGH skip to write logic
    ; We delay to ensure a valid address on the bus (required for GBC)
    ; TODO double check delay on GBC, is less than 16 possible?
    jmp pin handle_write [16]; DMG delay = 0, GBC delay = 16

handl_read:
    ; Read address, data and WR into ISR and auto push to C program
    in pins, 32

    ; Wait for data byte to be supplied by C program
    pull
    out pins, 8

    ; Put data byte on the bus by setting the direction of the output pins
    mov osr, ~null
    out pindirs, 32

ignore_read:
    wait 0 gpio PHI_PIN
    jmp bus_wait

handle_write:
    ; wait for WR to go low (+ some cycles to ensure valid data on the bus)
    ; TODO double check delay on DMG/GBC, is less than 16 possible?
    wait 0 gpio WR_PIN [16]; DMG delay = 16, GBC delay = 16

    ; Read address, data and WR into ISR and auto push to C program
    in pins, 32
.wrap


; Program for detecting cartridge ROM access
; ------------------------------------------

; 0x0000..=0x7FFF   0xxx_xxxx_xxxx_xxxx 0xA000 (cartridge ROM)
.program rom_access
.wrap_target
    ; Set IRQ and wait for bus program to clear it
    irq wait 1 [2]; delay in order to sync IRQ timings

    ; wait for cartridge ROM being accessed
    wait 0 gpio A15_PIN

    ; clear IRQ, waking up the bus program
    irq clear 0
.wrap


; Program for detecting cartridge RAM access
; ------------------------------------------

; 0xA000..=0xBFFF   101x_xxxx_xxxx_xxxx 0xA000 (cartridge RAM)
; 0xC000..=0xFDFF   11xx_xxxx_xxxx_xxxx 0xC000 (internal RAM, ignored)
.program ram_access
.wrap_target
ignore_internal_address:
    ; Set IRQ and wait for bus program to clear it
    irq wait 2

    ; wait for RAM being accessed
    wait 0 gpio CS_PIN

    ; Check if A14 is high (indicating internal RAM access)
    jmp pin ignore_internal_address

    ; clear IRQ, waking up the bus program
    irq clear 0
.wrap


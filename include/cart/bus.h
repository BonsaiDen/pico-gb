#ifndef _CART_BUS_H_
#define _CART_BUS_H_

// STD ------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#include "stdlib.h"


// External -------------------------------------------------------------------
// ----------------------------------------------------------------------------
#include "hardware/gpio.h"
#include "hardware/vreg.h"
#include "hardware/pio.h"


// Internal -------------------------------------------------------------------
// ----------------------------------------------------------------------------
#include "cart/cart.h"
#include "cart/bus.pio.h"


// Defines --------------------------------------------------------------------
// ----------------------------------------------------------------------------
// --- GPIO Layout (Pimoroni PGA2040 board) ---
//
// a = address (in)
// d = data (in/out)
// w = write (in)
// p = phi (in)
// r = reset (out)
//
// GPIO 0-15           GPIO 16-23 GPIO 24-26
// aaaa_aaaa_aaaa_aaaa dddd_dddd  wpr
//
// ----------------------------------------------------------------------------
#define DATA_PIN_0 16
#define A14_PIN 14
#define A15_PIN 15
#define WR_PIN 24
#define PHI_PIN 25
#define RESET_PIN 26
#define RD_PIN 27
#define CS_PIN 28
#define LED_PIN 29

#define PIO_BUS_PROGRAM 0
#define PIO_ROM_PROGRAM 1
#define PIO_BAM_PROGRAM 2


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
extern const uint32_t DATA_PIN_MASK;
static const uint32_t BUS_REQUEST_MASK = (1 << A15_PIN) | (1 << CS_PIN);


// Global Interface -----------------------------------------------------------
// ----------------------------------------------------------------------------

// Since the speed of executing from flash is too slow for running the hot
// mapper loops, these functions need to be put into the data segment (i.e. RAM)
extern __attribute__((hot, noreturn, section(".data"))) void mbc0(PIO pio);
extern __attribute__((hot, noreturn, section(".data"))) void mbc1(PIO pio);
extern __attribute__((hot, noreturn, section(".data"))) void mbc5(PIO pio);


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
struct BusState {
    uint32_t address;
    const uint8_t data;
    const bool read;
};


// Bus Helpers ----------------------------------------------------------------
// ----------------------------------------------------------------------------
inline static uint32_t bus_compute_rom_bank_offset(
    const struct BusState bus,
    const uint32_t rom_bank_offset
) {
    return (bus.address & ~0x4000) + rom_bank_offset;
}

inline static uint32_t bus_compute_ram_bank_offset(
    const struct BusState bus,
    const uint32_t ram_bank_offset
) {
    return (bus.address & ~0xA000) + ram_bank_offset;
}

inline static bool bus_is_rom_access(const struct BusState bus) {
    return (bus.address & 0x8000) == 0;
    //return bus.address < 0x8000;
}

inline static bool bus_is_banked_rom_access(const struct BusState bus) {
    // This works because bus.address will always be < 0x8000
    return (bus.address & 0x4000) != 0;
}

inline static void bus_release_reset() {
    gpio_put(RESET_PIN, 0);
}


// Bus Interface --------------------------------------------------------------
// ----------------------------------------------------------------------------
inline static struct BusState bus_wait_request(PIO pio) {
    struct BusState state;
    const uint32_t bits = pio_sm_get_blocking(pio, PIO_BUS_PROGRAM);
    state.address = (uint16_t)bits;
    *(uint8_t *)&state.data = bits >> 16;
    *(bool *)&state.read = !(bits >> RD_PIN & 0x1);
    return state;
}

inline static void bus_answer_read(PIO pio, const uint8_t byte) {
    pio_sm_put(pio, PIO_BUS_PROGRAM, byte);
}

#endif


#ifndef _EMULATION_H_
#define _EMULATION_H_

#include <stdbool.h>
#include <unistd.h>

void emulation_init(void);
void emulation_task(void);
void emulation_start(void);
void emulation_stop(void);
void emulation_reset(void);
void emulation_data(uint8_t *data);
void emulation_blink(const bool state);

#endif


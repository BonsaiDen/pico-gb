// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
#include <stdlib.h>


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "mem/mem.h"
#include "tusb.h"


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "usb/msc.h"
#include "util/print.h"


// Defines --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#define README_CONTENTS "README Contents."
#define DISK_READ_SECTORS                   4
#define DEFAULT_MAX_UPLOAD_SIZE_KB          32
#define DEFAULT_MAX_UPLOAD_SIZE_BYTES       (DEFAULT_MAX_UPLOAD_SIZE_KB * 1024)
#define DISK_BLOCK_NUM_FAKE                 (DEFAULT_MAX_UPLOAD_SIZE_KB * 2 + DISK_READ_SECTORS)
#define DISK_BLOCK_SIZE                     512


// Locals ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
static bool ejected = false; // whether host does safe-eject
static uint32_t max_upload_size_bytes = DEFAULT_MAX_UPLOAD_SIZE_BYTES;
static uint32_t disk_block_count = DISK_BLOCK_NUM_FAKE;

static uint32_t max_lba_written = 0;
static uint32_t bytes_written = 0;
static bool write_completed = false;

static uint32_t upload_file_size = 0;
static uint8_t *upload_file_buffer = NULL;
static char upload_file_name[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


// FAT Mock -------------------------------------------------------------------
// ----------------------------------------------------------------------------
uint8_t msc_disk[DISK_READ_SECTORS][DISK_BLOCK_SIZE] =
{
  //------------- Block0: Boot Sector -------------//
  {
      // 0x00: Dummy JUMP
      0xEB, 0x3C, 0x90,

      // 0x03: OEM Name
      0x4D, 0x53, 0x44, 0x4F, 0x53, 0x35, 0x2E, 0x30,

      // 0x0B: Bytes per Sector 512 / 1024 / 2048 / 4096 = DISK_BLOCK_SIZE
      0x00, (DISK_BLOCK_SIZE >> 8) & 0xFF,

      // 0x0D: Sectors per Cluster = 1
      0x01,

      // 0x0E: Size of reserved sectors = 1
      0x01, 0x00,

      // 0x10: Number of FATs = 1
      0x01,

      // 0x11: Root entries count (512)
      0x10, 0x00,

      // 0x13: Small number of sectors in the file system = DISK_BLOCK_NUM_FAKE
      DISK_BLOCK_NUM_FAKE & 0xFF, (DISK_BLOCK_NUM_FAKE >> 8) & 0xFF,

      // Media Type = fixed disk
      0xF8,

      // Size of each FAT in sectors = 1
      0x01, 0x00,

      // Sectors per track
      0x01, 0x00,

      // Number of heads
      0x01, 0x00,

      // Number of sectors before the start partition
      0x00, 0x00, 0x00, 0x00,

      // Number of sectors in the file system
      0x00, 0x00, 0x00, 0x00,

      // Drive Number = 0x80
      0x80,

      // IO Errors Encountered (Bit 1) / Dirty Flag (Bit 0)
      0x00,

      // Extended Boot Signature
      0x29,

      // Volume Serial Number
      0x34, 0x12, 0x00, 0x00,

      // 0x2B: Volume Label
      'V' , 'o' , 'l' , 'u' , 'm' , 'e' , 'L' , 'a' , 'b' , 'e' , 'l' ,

      // 0x36: FAT16 (Padded with Spaces)
      0x46, 0x41, 0x54, 0x31, 0x36, 0x20, 0x20, 0x20,

      // Unused
      0x00, 0x00,

      // Zero up to 2 last bytes of FAT magic code
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x55, 0xAA
  },

  //------------- Block1: FAT16 Table -------------//
  {
      // first entry must be F8(same as media type) FF
      0xF8, 0xFF,
      // end of file value (two highest bits allow for dirty setting)
      0xFF, 0xFF,
      // third entry is cluster end of readme file
      0x0F
  },

  //------------- Block2: Root Directory -------------//
  {
      // first entry is volume label
      'V' , 'o' , 'l' , 'u' , 'm' , 'e' , 'L' , 'a' , 'b' , 'e' , 'l' ,
      // Flag (Volumen Label)
      0x08,
      0x00, 0x00, 0x00, 0x00,
      0x00, 0x00,
      0x00, 0x00,
      0x00, 0x00,
      0x4F, 0x6D,
      0x65, 0x43,
      0x00, 0x00,
      0x00, 0x00, 0x00, 0x00,

      // second entry is readme file
      'R' , 'E' , 'A' , 'D' , 'M' , 'E' , ' ' , ' ' , 'T' , 'X' , 'T' ,
      // Flag (Read Only)
      0x01,
      0x00, 0xC6, 0x52, 0x6D,
      0x65, 0x43,

      0x65, 0x43,
      0x00, 0x00,

      // Last Modified Time
      0x88, 0x6D,

      // Last Modified Date
      0x65, 0x43,

      // Starting Cluster
      0x02, 0x00,

      // Size of file in bytes
      sizeof(README_CONTENTS)-1, 0x00, 0x00, 0x00 // readme's files size (4 Bytes)
  },

  //------------- Block3: Readme Content -------------//
  README_CONTENTS
};

uint32_t min(uint32_t a, uint32_t b) {
    if (a < b) {
        return a;

    } else {
        return b;
    }
}


// File Transfer Tasks --------------------------------------------------------
// ----------------------------------------------------------------------------
void msc_init(const char *volume_label, const char *readme_text, const uint32_t max_upload_size_kb) {
    // Reset state
    upload_file_size = 0;
    write_completed = false;
    max_lba_written = 0;
    bytes_written = 0;

    // Setup Volume Size
    max_upload_size_bytes = max_upload_size_kb * 1024;
    disk_block_count = max_upload_size_kb * 2 + DISK_READ_SECTORS;
    msc_disk[0][19] = disk_block_count & 0xff;
    msc_disk[0][20] = (disk_block_count >> 8) & 0xff;

    // Pre-alloc static upload buffer
    const char *readme_contents;
    upload_file_buffer = mem_alloc_rom_bytes();
    if (upload_file_buffer == NULL) {
        printf("[FAT] [ERROR] Failed to allocated %ld KiB of ROM buffer\n", max_upload_size_kb);
        readme_contents = "Buffer allocation failed";

    } else {
        readme_contents = readme_text;
    }

    // Setup Volume Label
    uint32_t volume_label_len = min(strlen(volume_label), 11);
    for(uint32_t i = 0; i < volume_label_len; i++) {
        msc_disk[0][42 + i] = volume_label[i];
        msc_disk[2][i] = volume_label[i];
    }

    // Setup README
    uint32_t readme_len = min(strlen(readme_contents), DISK_BLOCK_SIZE);
    for(uint32_t i = 0; i < readme_len; i++) {
        msc_disk[3][i] = readme_contents[i];
    }
    msc_disk[2][60] = readme_len & 0xff;
    msc_disk[2][61] = (readme_len >> 8) & 0xff;
    msc_disk[2][62] = (readme_len >> 16) & 0xff;
    msc_disk[2][63] = (readme_len >> 24) & 0xff;
}

void msc_task(void) {
    if (write_completed) {
        write_completed = false;
        if (upload_file_size > 0) {

            struct File file;
            file.size = upload_file_size;
            file.data = upload_file_buffer;

            // Extract Filename
            bool valid = false;
            for(int i = 7; i >= 0; i--) {
                if (upload_file_name[i] != ' ') {
                    valid = true;
                }
                if (valid) {
                    file.name[i] = upload_file_name[i];

                } else {
                    file.name[i] = '\0';
                }
            }
            file.name[8] = '\0';

            // Extract Extension
            valid = false;
            for(int i = 2; i >= 0; i--) {
                if (upload_file_name[8 + i] != ' ') {
                    valid = true;
                }
                if (valid) {
                    file.ext[i] = upload_file_name[8 + i];

                } else {
                    file.ext[i] = '\0';
                }
            }
            file.ext[3] = '\0';
            printf("[FAT] Transfer completed\n");
            msc_transfer_complete_callback(file);
        }
        upload_file_size = 0;
    }
}


// USB MSC Callbacks ----------------------------------------------------------
// ----------------------------------------------------------------------------
void tud_msc_inquiry_cb(uint8_t lun, uint8_t vendor_id[8], uint8_t product_id[16], uint8_t product_rev[4]) {
    // Invoked when received SCSI_CMD_INQUIRY
    // Application fill vendor id, product id and revision with string up to 8, 16, 4 characters respectively
    (void) lun;

    const char vid[] = "GameBoy";
    const char pid[] = "Mass Storage";
    const char rev[] = "1.0";
    memcpy(vendor_id  , vid, strlen(vid));
    memcpy(product_id , pid, strlen(pid));
    memcpy(product_rev, rev, strlen(rev));
}

bool tud_msc_test_unit_ready_cb(uint8_t lun) {
    // Invoked when received Test Unit Ready command.
    // return true allowing host to read/write this LUN e.g SD card inserted
    (void) lun;

    // RAM disk is ready until ejected
    if (ejected) {
        tud_msc_set_sense(lun, SCSI_SENSE_NOT_READY, 0x3a, 0x00);
        return false;

    } else {
        return true;
    }
}

void tud_msc_capacity_cb(uint8_t lun, uint32_t* block_count, uint16_t* block_size) {
    // Invoked when received SCSI_CMD_READ_CAPACITY_10 and SCSI_CMD_READ_FORMAT_CAPACITY to determine the disk size
    // Application update block count and block size
    (void) lun;
    *block_count = disk_block_count;
    *block_size  = DISK_BLOCK_SIZE;
}

bool tud_msc_start_stop_cb(uint8_t lun, uint8_t power_condition, bool start, bool load_eject) {
    // Invoked when received Start Stop Unit command
    // - Start = 0 : stopped power mode, if load_eject = 1 : unload disk storage
    // - Start = 1 : active mode, if load_eject = 1 : load disk storage
    (void) lun;
    (void) power_condition;

    if (load_eject) {
        if (start) {
            // load disk storage
        } else {
            // unload disk storage
            ejected = true;
            msc_ejected_callback();
        }
    }
    return true;
}

int32_t tud_msc_read10_cb(uint8_t lun, uint32_t lba, uint32_t offset, void* buffer, uint32_t bufsize) {
    // Callback invoked when received READ10 command.
    // Copy disk's data to buffer (up to bufsize) and return number of copied bytes.
    (void) lun;

    if (lba < DISK_READ_SECTORS) {
        uint8_t const* addr = msc_disk[lba] + offset;
        memcpy(buffer, addr, bufsize);
        return bufsize;

    } else {
        memset(buffer, 0, bufsize);
        return bufsize;
    }
}

int32_t tud_msc_write10_cb(uint8_t lun, uint32_t lba, uint32_t offset, uint8_t* buffer, uint32_t bufsize) {
    // Callback invoked when received WRITE10 command.
    // Process data in buffer to disk's storage and return number of written bytes
    (void) lun;

    // Check for boot sector writes (e.g. allow for setting clearing of dirty flag)
    if (lba == 0) {
        printf("[FAT] Writing boot sector...\n");
        uint8_t *addr = &msc_disk[0][offset];
        memcpy(addr, buffer, bufsize);

    // Check root directory block contents when written
    } else if (lba == 2) {
        printf("[FAT] Writing directory block...\n");
        if (max_lba_written >= DISK_READ_SECTORS) {
            if (bufsize == DISK_BLOCK_SIZE) {

                // Search Directory Block for the uploaded file
                uint32_t index = 0;
                while (index < DISK_BLOCK_SIZE) {

                    // Extract filename
                    memcpy(upload_file_name, &buffer[index], 11);

                    uint8_t flags = buffer[index + 11];
                    if (flags == 0x20) {
                        printf("[FAT] File \"%s\" (%d)\n", upload_file_name, flags);
                        // Check if written at expected starting cluster
                        uint8_t const *cluster_ptr = &buffer[index + 26];
                        uint32_t starting_cluster = cluster_ptr[0] | (cluster_ptr[1] << 8);
                        if (starting_cluster == 0x03) {
                            // Check that file size does not exceed rom buffer size
                            uint8_t const *size_ptr = &buffer[index + 28];
                            uint32_t file_size = size_ptr[0] | (size_ptr[1] << 8) | (size_ptr[2] << 16) | (size_ptr[3] << 24);
                            if (file_size <= max_upload_size_bytes) {
                                if (bytes_written >= file_size) {
                                    printf("[FAT] File written (%lu bytes)\n", file_size);
                                    upload_file_size = file_size;
                                }
                            }
                        }
                        break;
                    }
                    index += 32;
                }
            }
            bytes_written = 0;
            max_lba_written = 0;
            write_completed = true;
            printf("[FAT] Write completed\n");
        }

    // Check for additional sectors being written
    } else if (lba >= DISK_READ_SECTORS && upload_file_buffer != NULL) {

        // New sector is being written...
        if (lba > max_lba_written) {
            // ...re-direct it to the upload_file_buffer
            uint32_t rom_sector = lba - DISK_READ_SECTORS;
            if (rom_sector * DISK_BLOCK_SIZE + offset + bufsize <= max_upload_size_bytes) {
                if (bytes_written == 0) {
                    printf("[FAT] Transfer started...\n");
                    msc_transfer_start_callback();
                }
                uint8_t *addr = &upload_file_buffer[rom_sector * DISK_BLOCK_SIZE + offset];
                memcpy(addr, buffer, bufsize);
                bytes_written += bufsize;
                printf("[FAT] Block #%ld written (%ld total bytes)\n", rom_sector, bytes_written);
            }
            max_lba_written = lba;
        }
    }
    return bufsize;
}

void tud_msc_write10_complete_cb(uint8_t lun) {
    (void) lun;
}

int32_t tud_msc_scsi_cb(uint8_t lun, uint8_t const scsi_cmd[16], void* buffer, uint16_t bufsize) {
    // Callback invoked when received an SCSI command not in built-in list below
    // - READ_CAPACITY10, READ_FORMAT_CAPACITY, INQUIRY, MODE_SENSE6, REQUEST_SENSE
    // - READ10 and WRITE10 has their own callbacks
    // read10 & write10 has their own callback and MUST not be handled here
    void const* response = NULL;
    uint16_t resplen = 0;

    // most scsi handled is input
    bool in_xfer = true;
    switch (scsi_cmd[0]) {
        case SCSI_CMD_PREVENT_ALLOW_MEDIUM_REMOVAL:
            // Host is about to read/write etc ... better not to disconnect disk
            resplen = 0;
            break;

        default:
            // Set Sense = Invalid Command Operation
            tud_msc_set_sense(lun, SCSI_SENSE_ILLEGAL_REQUEST, 0x20, 0x00);

            // negative means error -> tinyusb could stall and/or response with failed status
            resplen = -1;
            break;
    }

    // return resplen must not larger than bufsize
    if (resplen > bufsize) {
        resplen = bufsize;
    }
    if (response && (resplen > 0)) {
        if (in_xfer) {
            memcpy(buffer, response, resplen);
        } else {
            // SCSI output
        }
    }
    return resplen;
}


// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
#include <stdlib.h>


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "tusb.h"
#include "bsp/board.h"
#include "pico/bootrom.h"


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "emulation.h"
#include "mem/mem.h"
#include "usb/msc.h"


// Definitions ----------------------------------------------------------------
// ----------------------------------------------------------------------------
enum {
    BLINK_NOT_MOUNTED = 100,
    BLINK_MOUNTED = 1000,
    BLINK_RUNNING = 500,
    BLINK_SUSPENDED = 2500,
    BLINK_IO = 25
};

void led_blinking_task(void);
void msc_transfer_start_callback();
void msc_transfer_complete_callback(struct File file);


// Locals ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
static bool rom_was_uploaded = false;
static bool usb_was_unmounted = false;
static uint32_t blink_interval_ms = BLINK_NOT_MOUNTED;
static uint32_t timer_connect_ms = 0;
static uint32_t timer_disconnect_ms = 0;


// USB MSC Interface ----------------------------------------------------------
// ----------------------------------------------------------------------------
void usb_init(void) {
    board_init();
    tusb_init();
    emulation_init();

    while (true) {
        emulation_task();

        // Wait for actual upload before starting disconnect timer
        if (rom_was_uploaded) {
            rom_was_uploaded = false;
            emulation_start();
            timer_disconnect_ms = board_millis();
        }
        if (timer_disconnect_ms != 0) {
            if (board_millis() - timer_disconnect_ms > 250) {
                timer_disconnect_ms = 0;
                tud_disconnect();
                // TODO figure out why tud_umount_cb is not getting called after SDK update
                blink_interval_ms = BLINK_NOT_MOUNTED;
                timer_connect_ms = board_millis();
            }
        }

        // Wait for actual unmount before connecting again
        if (usb_was_unmounted) {
            usb_was_unmounted = false;
            timer_connect_ms = board_millis();
        }
        if (timer_connect_ms != 0) {
            if (board_millis() - timer_connect_ms > 1000) {
                timer_connect_ms = 0;
                tud_connect();
            }

        } else {
            msc_task();
        }
        led_blinking_task();
        tud_task();
    }
}


// USB Interface --------------------------------------------------------------
// ----------------------------------------------------------------------------
void led_blinking_task(void) {
    static uint32_t start_ms = 0;
    static bool led_state = false;

    // Blink every interval ms
    if (board_millis() - start_ms < blink_interval_ms) return; // not enough time
    start_ms += blink_interval_ms;

    emulation_blink(led_state);
    led_state = 1 - led_state; // toggle
}


void msc_transfer_start_callback() {
    blink_interval_ms = BLINK_IO;
    emulation_stop();
}

void msc_transfer_complete_callback(struct File file) {
    emulation_data(file.data);
    rom_was_uploaded = true;
}

// Allow reboot into UF2 mode (in case there is no BOOTSEL available)
void msc_ejected_callback() {
    emulation_reset();
    reset_usb_boot(0, 0);
}

// Invoked when device is mounted
void tud_mount_cb(void) {
    blink_interval_ms = BLINK_MOUNTED;
}

// Invoked when device is unmounted
void tud_umount_cb(void) {
    blink_interval_ms = BLINK_NOT_MOUNTED;
    usb_was_unmounted = true;
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en) {
    (void) remote_wakeup_en;
    blink_interval_ms = BLINK_SUSPENDED;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void) {
    blink_interval_ms = BLINK_MOUNTED;
}

// Invoked when cdc when line state changed e.g connected/disconnected
void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts) {
    (void) itf;
    (void) rts;
    if (dtr) {
        printf("[GBDEV] Ready\n");
    }
}

// Invoked when CDC interface received data from host
void tud_cdc_rx_cb(uint8_t itf) {
    (void) itf;
}


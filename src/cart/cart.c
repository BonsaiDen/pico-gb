// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "hardware/sync.h"
#include "hardware/structs/bus_ctrl.h"


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "mem/mem.h"
#include "cart/bus.h"


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const uint32_t DATA_PIN_MASK = 0b111111110000000000000000;


// Global Storage -------------------------------------------------------------
// ----------------------------------------------------------------------------
uint8_t* ROM_DATA = NULL;
uint8_t* RAM_DATA = NULL;


// Internal -------------------------------------------------------------------
// ----------------------------------------------------------------------------
void _set_output_pin(const int pin) {
    gpio_init(pin);
    gpio_set_dir(pin, GPIO_OUT);
    gpio_put(pin, 0);
}

void _set_input_pin(const int pin) {
    gpio_init(pin);
    gpio_set_dir(pin, GPIO_IN);
}

void _config_pio_bus_program(PIO pio, uint sm, uint offset) {
    // Configure BUS program
    pio_sm_config c = bus_program_get_default_config(offset);

    // Enable everything as input pins
    sm_config_set_in_pins(&c, 0);
    sm_config_set_in_shift(&c, false, true, 32);

    // Enable data bus as output pins
    sm_config_set_out_pins(&c, DATA_PIN_0, 8);
    sm_config_set_out_shift(&c, true, false, 32);

    // Configure RD as the JMP pin
    sm_config_set_jmp_pin(&c, RD_PIN);

    // Initialize state machine
    pio_sm_init(pio, sm, offset, &c);
}

void _config_pio_rom_access_program(PIO pio, uint sm, uint offset) {
    pio_sm_config c = rom_access_program_get_default_config(offset);
    sm_config_set_in_pins(&c, 0);
    pio_sm_init(pio, sm, offset, &c);
}

void _config_pio_ram_access_program(PIO pio, uint sm, uint offset) {
    pio_sm_config c = ram_access_program_get_default_config(offset);
    sm_config_set_in_pins(&c, 0);
    sm_config_set_jmp_pin(&c, A14_PIN);
    pio_sm_init(pio, sm, offset, &c);
}

void _start_pio_programs(PIO pio) {
    // Removing the existing ones in case of a restart of the core
    pio_clear_instruction_memory(pio);

    // Expose data bus as output pins to PIO state machines
    for (int i = 16; i < 24; i++) {
        pio_gpio_init(pio, i);
    }

    // Configure state machines
    _config_pio_bus_program(pio, PIO_BUS_PROGRAM, pio_add_program(pio, &bus_program));
    _config_pio_rom_access_program(pio, PIO_ROM_PROGRAM, pio_add_program(pio, &rom_access_program));
    _config_pio_ram_access_program(pio, PIO_BAM_PROGRAM, pio_add_program(pio, &ram_access_program));

    // Enable state machines
    pio_sm_set_enabled(pio, PIO_BUS_PROGRAM, true);
    pio_sm_set_enabled(pio, PIO_ROM_PROGRAM, true);
    pio_sm_set_enabled(pio, PIO_BAM_PROGRAM, true);
}


// Lib Interface --------------------------------------------------------------
// ----------------------------------------------------------------------------
void cart_led_write(const uint8_t m) {
    gpio_put(LED_PIN, m);
}

void cart_init(void) {
    // Overclock
    vreg_set_voltage(VREG_VOLTAGE_1_30);
    sleep_ms(250);
    set_sys_clock_khz(400000, true);
    sleep_ms(250);

    // LED Status Pin
    _set_output_pin(LED_PIN);

    // Control pins
    _set_input_pin(PHI_PIN);
    _set_input_pin(WR_PIN);
    _set_input_pin(RD_PIN);
    _set_input_pin(CS_PIN);
    _set_output_pin(RESET_PIN);

    // Address Pins
    for(int i = 0; i < 16; i++) {
        _set_input_pin(i);
    }

    // Data Pins
    for(uint8_t i = 16; i < 24; i++) {
        _set_output_pin(i);

        // Increase slew rate for DATA pins for putting things faster on the bus
        gpio_set_slew_rate(i, GPIO_SLEW_RATE_FAST);
    }

    // Reset data bus
    cart_reset();
}

bool cart_is_running(void) {
    static uint32_t active = 0;
    static uint32_t last = 0;

    // Detection is done by looking for an active clock on the cartridge bus
    //
    // The time between clock edges depends on the CPU load of the GameBoy
    // The more time the GameBoy spends in HALT (i.e. the less CPU load there is),
    // the higher the time between two edges, in order to not incorrectly reset
    // during these HALTs we need to have a high enough counter below.
    bool current = gpio_get(PHI_PIN);
    if (current != last) {
        last = current;
        active += 1;
    }

    // Reduce counter until another edge transition is discovered
    if (active > 10) {
        return true;

    } else {
        return false;
    }
}

void cart_reset(void) {
    // Set the output of the data bus to 0xFF
    gpio_set_dir_out_masked(DATA_PIN_MASK);
    gpio_put_all(0xFF << 16);

    // Reset is done via pulling the reset line low via an inverter
    gpio_put(RESET_PIN, 1);
    sleep_ms(10);
    gpio_put(RESET_PIN, 0);
}

void cart_init_core(void) {

    // Disable Interrupts on this processor
    save_and_disable_interrupts();

    // Give core#1 priority on the bus since it needs to access SRAM
    bus_ctrl_hw->priority = BUSCTRL_BUS_PRIORITY_PROC1_BITS;

    // Allocate cartridge RAM
    uint8_t ram_size = ROM_DATA[0x149];

    // Allocate RAM for cartridge
    if (ram_size == 0x01) {
        RAM_DATA = mem_alloc_ram_bytes();

    } else if (ram_size == 0x02) {
        RAM_DATA = mem_alloc_ram_bytes();

    } else if (ram_size == 0x03) {
        RAM_DATA = mem_alloc_ram_bytes();

    } else {
        RAM_DATA = NULL;
    }

    // Hold reset line high during PIO setup in order to synchronize all state machines
    gpio_put(RESET_PIN, 1);

    // Start PIO programs
    PIO pio = pio0;
    _start_pio_programs(pio);

    // Hold reset line high long enough for gameboy to fully reset
    sleep_ms(10);

    // Select mapper implementation from cartridge header (MUST release reset line)
    uint8_t cartridge_type = ROM_DATA[0x0147];
    if (cartridge_type >= 0x01 && cartridge_type <= 0x03) {
        mbc1(pio);

    } else if (cartridge_type >= 0x19 && cartridge_type <= 0x1E) {
        mbc5(pio);

    } else {
        mbc0(pio);
    }
}


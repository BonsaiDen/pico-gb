// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "cart/bus.h"


// Memory Bank Controller Logic -----------------------------------------------
// ----------------------------------------------------------------------------
inline static uint8_t mbc_control_register(const struct BusState bus) {
    return bus.address >> 12;
}

__attribute__((hot, noreturn, section(".data"))) void mbc5(PIO pio) {

    // Mapper registers
    uint8_t reg_rom_bank = 1;
    uint8_t reg_ram_bank = 0;
    bool reg_ram_enable = false;

    // Bank offsets
    uint32_t rom_bank_offset = 0x4000;
    uint32_t ram_bank_offset = 0;

    bus_release_reset();
    while(true) {
        struct BusState bus = bus_wait_request(pio);
        if (bus.read) {
            if (bus_is_rom_access(bus)) {
                if (bus_is_banked_rom_access(bus)) {
                    bus.address = bus_compute_rom_bank_offset(bus, rom_bank_offset);
                }
                bus_answer_read(pio, ROM_DATA[bus.address]);

            } else if (reg_ram_enable) {
                bus_answer_read(pio, RAM_DATA[bus_compute_ram_bank_offset(bus, ram_bank_offset)]);
            }

        } else if (bus_is_rom_access(bus)) {
            switch (mbc_control_register(bus)) {
                case 0x0:
                case 0x1:
                    // Full 8 bits are compared
                    reg_ram_enable = (bus.data == 0x0A) && RAM_DATA != NULL;
                    break;

                case 0x2:
                    // Lower 8 bits
                    reg_rom_bank = bus.data | (reg_rom_bank & 0b100000000);
                    break;

                case 0x3:
                    // Upper 9th bit
                    reg_rom_bank = (bus.data & 0x1) << 9 | reg_rom_bank;
                    break;

                case 0x4:
                case 0x5:
                    reg_ram_bank = bus.data & 0xF;
                    break;
            }
            rom_bank_offset = (uint32_t)reg_rom_bank * 0x4000;
            ram_bank_offset = (uint32_t)reg_ram_bank * 0x2000;

        } else if (reg_ram_enable) {
            RAM_DATA[bus_compute_ram_bank_offset(bus, ram_bank_offset)] = bus.data;
        }
    }
}


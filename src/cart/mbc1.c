// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "cart/bus.h"


// Memory Bank Controller Logic -----------------------------------------------
// ----------------------------------------------------------------------------
inline static uint8_t mbc_control_register(const struct BusState bus) {
    return bus.address >> 13 & 3;
}

__attribute__((hot, noreturn, section(".data"))) void mbc1(PIO pio) {

    // Mapper registers
    uint8_t reg_rom_bank_low = 1;
    uint8_t reg_rom_bank_high = 0;
    uint8_t reg_bank_mode = 0;
    bool reg_ram_enable = false;

    // Bank offsets
    uint32_t rom_bank_offset = 0x4000;
    uint32_t ram_bank_offset = 0;

    bus_release_reset();
    while(true) {
        struct BusState bus = bus_wait_request(pio);
        if (bus.read) {
            if (bus_is_rom_access(bus)) {
                if (bus_is_banked_rom_access(bus)) {
                    bus.address = bus_compute_rom_bank_offset(bus, rom_bank_offset);

                } else if (reg_bank_mode == 0x01) {
                    bus.address = bus.address + rom_bank_offset;
                }
                bus_answer_read(pio, ROM_DATA[bus.address]);

            } else if (reg_ram_enable) {
                bus_answer_read(pio, RAM_DATA[bus_compute_ram_bank_offset(bus, ram_bank_offset)]);
            }

        } else if (bus_is_rom_access(bus)) {
            switch (mbc_control_register(bus)) {
                case 0:
                    // Only the lower 4bits are compared
                    reg_ram_enable = ((bus.data & 0xF) == 0x0A) && RAM_DATA != NULL;
                    break;

                case 1:
                    // A value of 0 cannot be written to this register
                    if (bus.data == 0) {
                        reg_rom_bank_low = 1;

                    } else {
                        reg_rom_bank_low = bus.data & 0b11111;
                    }
                    break;

                case 2:
                    // Bit 6 & 7 of rom bank / 2 bits of ram bank
                    reg_rom_bank_high = bus.data & 0b11;
                    break;

                case 3:
                    reg_bank_mode = bus.data & 0b1;
                    break;
            }
            if (reg_bank_mode == 0x01) {
                rom_bank_offset = ((uint32_t)reg_rom_bank_high << 5) * 0x4000;
                ram_bank_offset = (uint32_t)reg_rom_bank_high * 0x2000;

            } else {
                rom_bank_offset = ((uint32_t)reg_rom_bank_high << 5 | (uint32_t)reg_rom_bank_low) * 0x4000;
                ram_bank_offset = 0;
            }

        } else if (reg_ram_enable) {
            RAM_DATA[bus_compute_ram_bank_offset(bus, ram_bank_offset)] = bus.data;
        }
    }
}


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "cart/bus.h"


// Memory Bank Controller Logic -----------------------------------------------
// ----------------------------------------------------------------------------
__attribute__((hot, noreturn, section(".data"))) void mbc0(PIO pio) {
    bus_release_reset();
    while(true) {
        const struct BusState bus = bus_wait_request(pio);
        if (bus.read && bus_is_rom_access(bus)) {
            bus_answer_read(pio, ROM_DATA[bus.address]);
        }
    }
}


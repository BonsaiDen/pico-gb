// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
#include <stdlib.h>
#include <unistd.h>


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "mem/mem.h"


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
uint8_t* mem_alloc_rom_bytes() {
    static uint8_t* MEM_ROM = NULL;
    if (MEM_ROM == NULL) {
        MEM_ROM = (uint8_t*)sbrk(sizeof(uint8_t) * MEM_MAX_ROM_SIZE_KB * 1024);
    }
    return MEM_ROM;
}

uint8_t* mem_alloc_ram_bytes() {
    static uint8_t* MEM_RAM = NULL;
    if (MEM_RAM == NULL) {
        MEM_RAM = (uint8_t*)sbrk(sizeof(uint8_t) * MEM_MAX_RAM_SIZE_KB * 1024);
    }
    return MEM_RAM;
}


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "emulation.h"
#include "cart/cart.h"
#include "mem/mem.h"
#include "usb/msc.h"
#include "util/print.h"


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
#include "pico/multicore.h"


// Statics --------------------------------------------------------------------
// ----------------------------------------------------------------------------
static bool gameboy_running = false;
static bool emulation_running = false;


// High Level Emulation Logic -------------------------------------------------
// ----------------------------------------------------------------------------
void emulation_init(void) {
    cart_init();
    msc_init("GameBoy DEV", "Upload a ROM by dropping a *.gb(c) file.", MEM_MAX_ROM_SIZE_KB);
}

void emulation_start(void) {
    // Only start if a ROM is actually present and the gameboy is powered on
    if (gameboy_running && ROM_DATA != NULL) {
        emulation_running = true;

        // (Re-)Launch Core#1 with the actual emulation logic
        multicore_launch_core1(cart_init_core);
        printf("[GBDEV] Started cartridge emulation\n");
    }
}

void emulation_stop(void) {
    if (emulation_running) {
        // Stop Core#1 with the actual emulation logic
        multicore_reset_core1();
        cart_reset();
        emulation_running = false;
        printf("[GBDEV] Stopped\ cartridge emulation");
    }
}

void emulation_task(void) {
    if (cart_is_running()) {
        if (!gameboy_running) {
            printf("[GBDEV] GameBoy powered on\n");
            gameboy_running = true;
            emulation_start();
        }
    }/* else if (gameboy_running) {
        printf("[GAMEBOY] Powered off\n");
        gameboy_running = false;
        emulation_stop();
    }*/
}

void emulation_reset(void) {
    cart_reset();
}

void emulation_data(uint8_t *data) {
    ROM_DATA = data;
}

void emulation_blink(const bool state) {
    cart_led_write(state);
}

